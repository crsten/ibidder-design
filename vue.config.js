const path = require('path')

module.exports = {
  outputDir: '../design',
  baseUrl: '',
  configureWebpack: {
    resolve: {
      alias: {
        node_modules: path.resolve('node_modules'),
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "./src/styles/base/variables";
        `,
      },
    },
  },
}
