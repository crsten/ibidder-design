import Vue from 'vue'
import App from './App.vue'
import './styles/main.scss'
import Bus from './bus'
import './api'

Vue.config.productionTip = false

import ColorDetector from './utils/color-detector'

new Vue({
  data() {
    return {
      title: 'Auksjon',
      pattern: 'wiggle',
      theme: '#33658A',
      visibleBids: 5,
      patterns: [
        'clean',
        'endless-clouds',
        'bank-note',
        'leaf',
        'wiggle',
        'jigsaw',
        'autumn',
        'bathroom-floor',
        'overlapping-circles',
        'tic-tac-toe',
        'texture',
      ],
      confetti: false,
    }
  },
  computed: {
    textColor() {
      return ColorDetector(this.theme)
    },
  },
  mounted() {
    Bus.$on('auction:done', (state = true) => {
      this.confetti = state
    })
  },
  render: h => h(App),
}).$mount('#app')
