import Bus from './bus'
import nanoid from 'nanoid'

window.startAuction = function(state = false) {
  Bus.$emit('lobby:toggle', state)
}

window.addBid = function(name, value, happyhour, total) {
  Bus.$emit('bid:add', { name, value, happyhour, total, id: nanoid() })
  Bus.$emit('total:update', total)
}

window.auctionEnded = function() {
  Bus.$emit('auction:done')
  Bus.$emit('countdown', { current: 0, total: 0 })
}

window.drawWinner = function(name) {
  Bus.$emit('winner:show', name)
}

window.closeWinner = function(name) {
  Bus.$emit('winner:hide')
}

window.stopAuction = function(current, total) {
  Bus.$emit('countdown', { current, total })
}

window.startHappyHour = function() {
  Bus.$emit('happyhour:start')
}

window.stopHappyHour = function() {
  Bus.$emit('happyhour:stop')
}
